<?php

namespace App\Services;

use Illuminate\Support\Collection;

class CookieService
{
    private const LINE_REGEX = '/(\S+):.* (-?\d+).* (-?\d+).* (-?\d+).* (-?\d+).* (-?\d+).*/m';

    private const TEA_SPOONS = 100;

    private const TOTAL_CALORIES = 500;

    private int|float $part1 = 0;
    private int|float $part2 = 0;

    /**
     * @param Collection $ingredients
     * @return array
     */
    public function findBestIngredientsMix(Collection $ingredients): array
    {
        $capacityList = $durabilityList = $flavorList = $textureList = $caloriesList = [];

        foreach ($ingredients as $key => $line) {
            if (preg_match_all(self::LINE_REGEX, $line, $matches)) {
                [$detail, $ingredient, $capacity, $durability, $flavor, $texture, $calories] = $matches;

                $capacityList[$key] = (int) $capacity[0];
                $durabilityList[$key] = (int) $durability[0];
                $flavorList[$key] = (int) $flavor[0];
                $textureList[$key] = (int) $texture[0];
                $caloriesList[$key] = (int) $calories[0];
            }
        }

        for ($i = 0; $i <= self::TEA_SPOONS; $i++) { // first ingredient
            for ($j = 0; $j <= (self::TEA_SPOONS - $i); $j++) { // second ingredient
                for ($k = 0; $k <= (self::TEA_SPOONS - $i - $j); $k++) { // third ingredient
                    $l = self::TEA_SPOONS - $i - $j - $k; // fourth ingredient

                    $totalCapacity = ($i * $capacityList[0])
                        + ($j * $capacityList[1])
                        + ($k * $capacityList[2])
                        + ($l * $capacityList[3]);

                    $totalDurability = ($i * $durabilityList[0])
                        + ($j * $durabilityList[1])
                        + ($k * $durabilityList[2])
                        + ($l * $durabilityList[3]);

                    $totalFlavor = ($i * $flavorList[0])
                        + ($j * $flavorList[1])
                        + ($k * $flavorList[2])
                        + ($l * $flavorList[3]);

                    $totalTexture = ($i * $textureList[0])
                        + ($j * $textureList[1])
                        + ($k * $textureList[2])
                        + ($l * $textureList[3]);

                    $totalScore = ((0 < $totalCapacity) ? $totalCapacity : 1)
                        * ((0 < $totalDurability) ? $totalDurability : 1)
                        * ((0 < $totalFlavor) ? $totalFlavor : 1)
                        * ((0 < $totalTexture) ? $totalTexture : 1);

                    if ($totalScore > $this->part1) {
                        $this->part1 = $totalScore;
                    }

                    $totalCalories = ($i * $caloriesList[0])
                        + ($j * $caloriesList[1])
                        + ($k * $caloriesList[2])
                        + ($l * $caloriesList[3]);

                    if (self::TOTAL_CALORIES == $totalCalories) {
                        if ($totalScore > $this->part2) {
                            $this->part2 = $totalScore;
                        }
                    }
                }
            }
        }

        return [$this->part1, $this->part2];
    }
}
