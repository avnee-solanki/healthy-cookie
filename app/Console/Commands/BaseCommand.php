<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

abstract class BaseCommand extends Command
{
    protected Collection $puzzleCollection;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function loadInput(string $fileName)
    {
        $puzzleInput = File::get(storage_path() . '/inputs/real/' . $fileName . '.txt');
        $this->puzzleCollection = Str::of($puzzleInput)->explode(PHP_EOL);
    }
}
