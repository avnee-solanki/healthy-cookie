<?php

namespace App\Console\Commands;

use App\Services\CookieService;

class HealthyCookie extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'healthy:cookie {part?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare a balanced cookie recipe';

    public function __construct(
        protected CookieService $cookieService
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $puzzlePart = $this->argument('part');

        $this->loadInput('input');

        [$part1, $part2] = $this->cookieService->findBestIngredientsMix($this->puzzleCollection);

        if (1 == $puzzlePart) {
            $this->info('Part 1:' . $part1);
        } else if (2 == $puzzlePart) {
            $this->info('Part 2:' . $part2);
        } else {
            $this->info('Part 1:'. $part1);
            $this->info('Part 2:'. $part2);
        }
    }
}
