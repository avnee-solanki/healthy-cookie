# Healthy-Cookie

## Description
[ read description here : advent of code - day 15 - 2015 puzzle](advent_of_code_15_2015.md)

## Getting started with set up

### Requirements
- PHP >= 8.0
- Composer

### Install Composer

[download and install composer](https://getcomposer.org/download/)

### Clone Repository
open your terminal, go to the directory that you will install this project, then run the following command:

```bash
git clone https://gitlab.com/avnee-solanki/healthy-cookie.git

cd healthy-cookie

composer install
```
you can run below command to get output of part-1 or part-2
````
php artisan healthy:cookie {?1|2}
```
